from class_thing import *
import yaml



class Box(object):

	def __init__(self,capacity=None,etat=False, contents=[],name=None):
		self.__contents=contents
		self.__open=etat
		self.__capacity=capacity
		self.__name=name
		self.__key=None

	def add(self, objet):
		'''
		ajoute un objet à la boite
		'''
		self.__contents.append(objet)

	def contain(self, element):
		'''
		retourne la liste des éléments contenus dans la boite
		'''
		return element in self.__contents

	def delete(self,objet):
		'''
		supprime l'objet de la boite
		'''
		if self.contain(objet):
			self.__contents.remove(objet)

	def is_open(self):
		'''
		retourne un booléen qui indique si la boite est ouverte ou non
		'''
		return self.__open

	# def open(self):
	# 	'''
	# 	ouvre la boite   ///  Remplacée par open_with
	# 	'''
	# 	self.__open=True

	def close(self):
		'''ferme la boite
		'''
		self.__open=False

	def action_look(self):
		'''
		regarde dans la boite
		'''
		if self.is_open():
			return "La boite contient : "+", ".join(self.__contents)
		else :
			return "La boite est fermée"

	def set_capacity(self,volume):
		'''
		modifie la capacité de la boite
		'''
		self.__capacity=volume


	def capacity(self):
		'''
		renvoie la capacité de la boite
		'''
		return self.__capacity

	def has_room_for(self,objet):
		'''
		retourne un booléen qui indique s'il y a de la place ou non pour l'objet
		'''
		if self.__capacity==None or self.__capacity>=objet.volume():
			return True
		else :
			return False

	def action_add(self,objet):
		'''
		ajoute l'objet à la boite si c'est possible et retourne un booléen qui indique si l'objet a été placé dans le boite
		'''
		if self.has_room_for(objet) and self.is_open():
			if self.capacity()!=None:
				self.__capacity-=objet.volume()
			self.add(objet)
			return True
		else:
			return False

	def find(self,nomObjet):
		'''
		retourne le nom de l'objet s'il est dans la boite
		'''
		res=None
		if self.is_open():
			for objet in self.__contents:
				if objet.has_name(nomObjet):
					res=objet
					break
		return res

	def __repr__(self):
		'''
		modifie la représentation de la boite
		'''
		if self.is_open():
			return "<Nom : %s, Boite Ouverte, Contenu : %s, Capacité : %s>" %(self.__name,self.__contents,self.__capacity)
		else :
			return "<Nom : %s, Boite Fermée>" %(self.__name)
			
	def set_name_box(self,name):
		'''
		modifie le nom de la boite
		'''
		self.__name=name


	def set_key(self,t):
		self.__key=t


	def open_with(self,t=None):
		if self.__key==None or t==self.__key:
			self.__open=True



	@staticmethod
	def from_yaml(data):
		contenu = yaml.load(open(data))
		res=[]
		for i in range(len(contenu)):
			res.append(Box ( capacity=contenu[i]['capacity'], etat=contenu[i]['is_open'], contents=contenu[i]['contents'], name=contenu[i]['name']))
		return res



