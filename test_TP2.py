from class_box import *
from class_thing import *

def test_creation_boite():
	B=Box()
	B.add("Pièce")
	B.add("Epée")
	assert B.contain("Pièce")
	assert not B.contain("Casque")
	B.delete("Epée")
	assert not B.contain("Epée")
	assert not B.is_open()
	B.open_with()
	assert B.is_open()
	B.close()
	assert not B.is_open()
	B.add("Armure")
	assert B.action_look()=='La boite est fermée'
	B.open_with()
	assert B.action_look()=='La boite contient : Pièce, Armure'

def test_capacite_boite():
	B=Box()
	Sword=Thing(5)
	B.open_with()
	assert B.has_room_for(Sword)
	assert B.capacity()==None
	B.set_capacity(4)
	assert B.capacity()==4
	assert not B.has_room_for(Sword)
	Clou=Thing(2)
	assert B.action_add(Clou)
	assert not B.action_add(Sword)
	assert B.action_add(Clou)
	assert not B.action_add(Clou)
	B.close()
	assert not B.action_add(Clou)


def test_thing():
	Sword=Thing(3)
	Helmet=Thing(10)
	assert Sword.volume()==3
	assert Helmet.volume()==10
	Sword.set_name("Epée")
	assert Sword.has_name("Epée")
	assert not Sword.has_name("Bouclier")


def test_nom():
	B=Box()
	Sword=Thing(10,"Epée")
	B.open_with()
	Sword.set_name("Epée")
	B.add(Sword)
	#assert B.find("Epée")==Sword
	B.close()
	assert not B.find("Epée")==Sword


def test_nom_boite():
	B=Box()
	B.set_name_box("Coffre")
	B.open_with()
	B.set_capacity(4)
	Clou=Thing(2,"Clou")
	B.add(Clou)
	
def test_yaml():
	listeObj=[Box(3,True,["Pierre","Feuille","Ciseaux"],"Shifumi"),Box(5,False,[],"Coffre Test")]
	listYaml=Box.from_yaml("data.yaml")
	assert listeObj[0].is_open()==listYaml[0].is_open()
	assert listeObj[0].capacity()==listYaml[0].capacity()
	assert listeObj[1].capacity()==listYaml[1].capacity()



def test_ouverture_boite():
	b=Box(60)
	b.open_with()
	assert b.is_open()
	b.close()
	assert not b.is_open()
	cle=Thing(1,"Clé en platine")
	b.set_key(cle)
	b.open_with()
	assert not b.is_open()
	b.open_with(cle)
	assert b.is_open()
	b.close()
	assert not b.is_open()

	





	