

class Thing(object):

	def __init__(self,volume,name=None):
		self.__place=volume
		self.__name=name

	def volume(self):
		'''
		retourne le volume de l'objet
		'''
		return self.__place
		
	def set_name(self,name):
		'''
		modifie le nom de l'objet
		'''
		self.__name=name		

	def __repr__(self):
		'''
		modifie la représentation de l'objet
		'''
		return "Nom de l'objet : %s, volume : %s" %(self.__name,self.__place)

	def has_name(self,name):
		return self.__name==name


		